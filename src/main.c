
#include "main.h"
#include "math.h"


TIM_HandleTypeDef    TimHandle_input;
TIM_HandleTypeDef    TimHandle_output;

/* Timer Input Capture Configuration Structure declaration */
TIM_IC_InitTypeDef       sConfig_input;
TIM_SlaveConfigTypeDef   sSlaveConfig;

//TIM_OC_InitTypeDef sConfig;

/* Captured Value */
__IO uint32_t            uwIC2Value = 0;
/* Duty Cycle Value */
__IO uint32_t            uwDutyCycle = 0;
/* Frequency Value */
__IO uint32_t            uwFrequency = 255;

int frequency_buffer_index = 0;
uint32_t frequency_buffer[16]; /* frequency calulator buffer */

static uint32_t TimOutClock = 1;
uint32_t uhPrescalerValue = 0;
int pulseFreq = 0; 
int state = 0; 
int pulseFreq_state = 0;
long timer = 0;


TIM_OC_InitTypeDef sConfig_output;

/* Private function prototypes -----------------------------------------------*/
void                     SystemClock_Config(void);
static void              Error_Handler(void);
void                     Print_Info(uint8_t *String, uint32_t Size);
void                     Configure_USART(void);
void                     HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim);
void                     Start_Pulse(uint32_t frequency);
void                     Print_Number(int number);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
  HAL_Init();

  /* Configure the system clock to 80 MHz */
  SystemClock_Config();

  /* Configure LED2 */
  BSP_LED_Init(LED2);

  Configure_USART();

  Print_Info("reset", sizeof("reset"));

  /* Set TIMx instance */
  TimHandle_input.Instance = TIMx;

  TimHandle_input.Init.Period            = 0xFFFF;
  TimHandle_input.Init.Prescaler         = 0;
  TimHandle_input.Init.ClockDivision     = 0;
  TimHandle_input.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle_input.Init.RepetitionCounter = 0;
  if (HAL_TIM_IC_Init(&TimHandle_input) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-2- Configure the Input Capture channels ###############################*/
  /* Common configuration */
  sConfig_input.ICPrescaler = TIM_ICPSC_DIV1;
  sConfig_input.ICFilter = 0;

  /* Configure the Input Capture of channel 1 */
  sConfig_input.ICPolarity = TIM_ICPOLARITY_FALLING;
  sConfig_input.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&TimHandle_input, &sConfig_input, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }

  /* Configure the Input Capture of channel 2 */
  sConfig_input.ICPolarity = TIM_ICPOLARITY_RISING;
  sConfig_input.ICSelection = TIM_ICSELECTION_DIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&TimHandle_input, &sConfig_input, TIM_CHANNEL_2) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }
  /*##-3- Configure the slave mode ###########################################*/
  /* Select the slave Mode: Reset Mode  */
  sSlaveConfig.SlaveMode        = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger     = TIM_TS_TI2FP2;
  sSlaveConfig.TriggerPolarity  = TIM_TRIGGERPOLARITY_NONINVERTED;
  sSlaveConfig.TriggerPrescaler = TIM_TRIGGERPRESCALER_DIV1;
  sSlaveConfig.TriggerFilter    = 0;

  if (HAL_TIM_SlaveConfigSynchronization(&TimHandle_input, &sSlaveConfig) != HAL_OK)
  {
   
    Error_Handler();
  }

  /*##-4- Start the Input Capture in interrupt mode ##########################*/
  if (HAL_TIM_IC_Start_IT(&TimHandle_input, TIM_CHANNEL_2) != HAL_OK)
  {
    
    Error_Handler();
  }

  while (1)
  {
  }
}

void Start_Pulse(uint32_t frequency){
  TimHandle_output.Instance = TIM1;

  uint32_t periodValue = 500;
  uhPrescalerValue = (SystemCoreClock / frequency) / periodValue;

  TimHandle_output.Init.Prescaler         = uhPrescalerValue;
  TimHandle_output.Init.Period            = periodValue; 

  TimHandle_output.Init.ClockDivision     = 0;   
  TimHandle_output.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle_output.Init.RepetitionCounter = 0;
  

  if (HAL_TIM_PWM_Init(&TimHandle_output) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  sConfig_output.OCMode       = TIM_OCMODE_PWM1;
  sConfig_output.OCPolarity   = TIM_OCPOLARITY_HIGH;
  sConfig_output.OCFastMode   = TIM_OCFAST_DISABLE;
  sConfig_output.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
  sConfig_output.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  sConfig_output.OCIdleState  = TIM_OCIDLESTATE_RESET;
  sConfig_output.Pulse = 1;


  if (HAL_TIM_PWM_ConfigChannel(&TimHandle_output, &sConfig_output, TIM_CHANNEL_4) != HAL_OK)
    {
      Error_Handler();
    }

    if (HAL_TIM_PWM_Start(&TimHandle_output, TIM_CHANNEL_4) != HAL_OK)
    {
      Error_Handler();
    } 

}


void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{

  timer++;
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
  {
    /* Get the Input Capture value */
    uwIC2Value = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);

    if (uwIC2Value != 0)
    {
      /* Duty cycle computation */
      uwDutyCycle = ((HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1)) * 100) / uwIC2Value;

      /* uwFrequency computation
      TIM3 counter clock = (RCC_Clocks.HCLK_Frequency) */
      uwFrequency = (HAL_RCC_GetHCLKFreq())  / uwIC2Value;

      frequency_buffer[frequency_buffer_index] = uwFrequency;
      if(frequency_buffer_index == 15){
        uint32_t temp = 0;
        int adjustment = 0;
        for(int i = 0; i < 16; i++){
          temp += frequency_buffer[i];
        }

        pulseFreq = temp / 16;

        Print_Info("Current frequency  ", sizeof("Current frequency"));
        Print_Number(pulseFreq);
        Print_Info("Current state  ", sizeof("Current state"));
        Print_Number(state);
        Print_Number(timer);

        frequency_buffer_index = 0;
    
        if(pulseFreq_state == state || (timer % 125) == 0){

          int temp = 0;
          
          switch(state) {
             case 0:
                temp = pulseFreq;
                break;
             case 1:
                temp = pulseFreq+50;
                break;
             case 2:
                temp = pow(pulseFreq, 1.05);
                break;
             case 3:
                temp = pulseFreq/M_PI_2;
                break;
             case 4:
                temp = pulseFreq >> 1;
                break;
             case 5:
                temp = pulseFreq / (((1 + pow(5, 0.5)) / 2) / 2);
                break;
             case 6:
                state = 0;
                break;
          }
          temp = temp + adjustment + 10; /*frequency adjustment*/
          Start_Pulse(temp);

          pulseFreq_state++;

        }
        if(timer > 500 && timer < 1000){
          adjustment = 5;
        }
        if(timer > 1000 && timer < 1500){
          adjustment = -5;
        }
        if(timer > 1500){
          state++;
          adjustment = 0;
          timer = 0;
        }

      }else{
        frequency_buffer_index++;
      }

    }else{
      uwDutyCycle = 0;
      uwFrequency = 0;
    }
  }
}

void Print_Number(int number){
  char buffer[8];
  for(int i = 0; i < 7; i++){
    buffer[i] = ' ';
  }
  buffer[7] = 13;
  itoa(number, buffer, 10);
  Print_Info(buffer, sizeof(buffer));
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* Turn LED2 on */
  BSP_LED_On(LED2);
  while (1)
  {
  }
}


void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLR = 8;  //default = 2
  RCC_OscInitStruct.PLL.PLLP = 7;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV8;  //default = div1
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}


void Configure_USART(void)
{
  /* Configure UART through USB */

  USARTx_GPIO_CLK_ENABLE();

  LL_GPIO_SetPinMode(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_MODE_ALTERNATE);
  USARTx_SET_TX_GPIO_AF();
  //LL_GPIO_SetAFPin_0_7(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_AF_7);
  LL_GPIO_SetPinSpeed(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
  LL_GPIO_SetPinPull(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_PULL_UP);

  /* Configure Rx Pin as : Alternate function, High Speed, Push pull, Pull up */
  LL_GPIO_SetPinMode(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_MODE_ALTERNATE);

  USARTx_SET_RX_GPIO_AF();
 
  LL_GPIO_SetPinSpeed(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
  LL_GPIO_SetPinPull(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_PULL_UP);

  NVIC_SetPriority(USARTx_IRQn, 0);  
  NVIC_EnableIRQ(USARTx_IRQn);

  USARTx_CLK_ENABLE();

  /* Set clock source */
  USARTx_CLK_SOURCE();

  LL_USART_SetTransferDirection(USARTx_INSTANCE, LL_USART_DIRECTION_TX_RX);

  /* 8 data bit, 1 start bit, 1 stop bit, no parity */
  LL_USART_ConfigCharacter(USARTx_INSTANCE, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);

  
  LL_USART_SetBaudRate(USARTx_INSTANCE, HAL_RCC_GetHCLKFreq(), LL_USART_OVERSAMPLING_16, 9600); 

  /* (4) Enable USART *********************************************************/
  LL_USART_Enable(USARTx_INSTANCE);

  /* Polling USART initialisation */
  while((!(LL_USART_IsActiveFlag_TEACK(USARTx_INSTANCE))) || (!(LL_USART_IsActiveFlag_REACK(USARTx_INSTANCE))))
  { 
  }

  LL_USART_EnableIT_RXNE(USARTx_INSTANCE);
  LL_USART_EnableIT_ERROR(USARTx_INSTANCE);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
  GPIO_InitTypeDef   GPIO_InitStruct;
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM1_CLK_ENABLE();

  /* Enable GPIO Channels Clock */
  TIM1_CHANNEL_GPIO_PORT();

  /* Common configuration for all channels */
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  GPIO_InitStruct.Alternate = TIM1_GPIO_AF_CHANNEL4;
  GPIO_InitStruct.Pin = TIM1_GPIO_PIN_CHANNEL4;
  HAL_GPIO_Init(TIM1_GPIO_PORT_CHANNEL4, &GPIO_InitStruct);
}


void Print_Info(uint8_t *String, uint32_t Size)
{
  uint32_t index = 0;
  uint8_t *pchar = String;
  
  /* Send characters one per one, until last char to be sent */
  for (index = 0; index < Size; index++)
  {
    /* Wait for TXE flag to be raised */
    while (!LL_USART_IsActiveFlag_TXE(USARTx_INSTANCE))
    {
    }

    /* Write character in Transmit Data register.
       TXE flag is cleared by writing data in TDR register */
    LL_USART_TransmitData8(USARTx_INSTANCE, *pchar++);
  }

  /* Wait for TC flag to be raised for last char */
  while (!LL_USART_IsActiveFlag_TC(USARTx_INSTANCE))
  {
  }
}

void HAL_TIM_IC_MspInit(TIM_HandleTypeDef *htim)
{
  GPIO_InitTypeDef   GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIMx_CLK_ENABLE();

  /* Enable GPIO channels Clock */
  TIMx_CHANNEL_GPIO_PORT();

  /* Configure  (TIMx_Channel) in Alternate function, push-pull and High speed */
  GPIO_InitStruct.Pin = GPIO_PIN_CHANNEL2;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF_TIMx;
  HAL_GPIO_Init(GPIO_PORT, &GPIO_InitStruct);

  /*##-2- Configure the NVIC for TIMx #########################################*/

  HAL_NVIC_SetPriority(TIMx_IRQn, 0, 1);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIMx_IRQn);
}

void USARTx_IRQHandler(void)
{
  /* Check RXNE flag value in ISR register */
  if(LL_USART_IsActiveFlag_RXNE(USARTx_INSTANCE) && LL_USART_IsEnabledIT_RXNE(USARTx_INSTANCE))
  {
  }
}

void SysTick_Handler(void)
{
  HAL_IncTick();
}

void TIMx_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_input);
}

